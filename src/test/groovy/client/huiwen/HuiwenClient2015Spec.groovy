package client.huiwen

import spock.lang.*


class Huiwen2015Spec extends Specification {

    String url = 'http://10.1.123.244:8081/'
    String token = '1H1KJ4OCBJPJ0AFLM7E20GMNMH35OB8E9KG'



    def "获取读者信息"() {
        setup:
            def client = new HuiwenClient2015(url, token)

        when:
			def r = client.getReader('6120120098')

        then:
            r.get("success") == "1"
    }

	def "获取借阅信息"() {
        setup:
            def client = new HuiwenClient2015(url, token)

        when:
			def r = client.getMyLibrary('6120120096')

        then:
            r.get("success") == "1"
    }
	
	def "书目检索"() {
        setup:
            def client = new HuiwenClient2015(url, token)

        when:
			def r = client.queryBook('03','中国',1)

        then:
            r.get("success") == "1"
    }
	
	def "获取图书信息"() {
        setup:
            def client = new HuiwenClient2015(url, token)

        when:
			def r = client.getBookInfo('0000838604')

        then:
            r.get("success") == "1"
    }
	
	def "验证读者信息"() {
        setup:
            def client = new HuiwenClient2015(url, token)

        when:
			def r = client.rLogin('3120140392','3120140392')

        then:
            r.get("success") == "1"
    }
	

}
