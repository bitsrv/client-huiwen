package client.huiwen

import spock.lang.*


class Huiwen2012Spec extends Specification {

    String url = 'http://localhost:8081/HWWebService/LibServicePort'
    String un = 'libsys'
    String token = 'huiwen'

    def "接口权限错误"() {
        setup:
            def client = new HuiwenClient2012(url, un, 'wrong')

        when:
            client.getReader(1, '2015010097')

        then:
            wslite.soap.SOAPFaultException ex = thrown()
            ex.message == 'S:Server - 没有权限：校验失败！'
    }

    def "获取读者信息"() {
        setup:
            def client = new HuiwenClient2012(url, un, token)

        when:
            def r = client.getReader(1, '2015010097')
            print r

        then:
            r.name == '刘志伟'
    }

	
	def "获取书刊信息"() {
        setup: "a new stack instance is created"        
            def client = new HuiwenClient2012(url, un, token)

        when:                                           
            def r = client.getBook(2, '2056286')
            print r

        then:                                           
            r.title == '无线网络黑客攻防:畅销版'
    }
	
	def "获取读者借阅信息"() {
        setup: "a new stack instance is created"        
            def client = new HuiwenClient2012(url, un, token)

        when:                                           
            def r = client.getCircs('2015010096')
            print r

        then:                                           
            r.name == '王康'
    }
	
	

	
	def "获取超期欠款信息"() {
        setup: "a new stack instance is created"        
            def client = new HuiwenClient2012(url, un, token)

        when:                                           
            def r = client.getDebts('2015010094')
            print r

        then:                                           
            r.name == '蔡飞超'
    }
	
	

}
