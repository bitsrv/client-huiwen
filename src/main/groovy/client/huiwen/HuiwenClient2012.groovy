package client.huiwen

import java.security.MessageDigest
import wslite.soap.SOAPClient

class HuiwenClient2012 {

    String url
    String un
    String token
    
    def sdf
    def soapClient

    HuiwenClient2012(String url, String un, String token) {
        this.url = url
        this.un = un
        this.token = token

        this.sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm")
        soapClient = new SOAPClient(url)
    }

    private String generateMD5(String str) {
        MessageDigest digest = MessageDigest.getInstance("MD5")
        digest.update(str.bytes)
        return new BigInteger(1, digest.digest()).toString(16).padLeft(32, '0')
    }

    private String sdate() {
        sdf.format(new Date())
    }

    private String verify(String sDate) {
        String verify = this.un + this.token + sDate
        return generateMD5(verify).toLowerCase()
    }

    private def convertToMap(nodes) {
        nodes.children().collectEntries { 
            [ it.name(), it.childNodes() ? convertToMap(it) : it.text() ] 
        }
    }

    def getReader(int type, String readerId) {
        String d = sdate()
        String v = verify(d)
        
        def response = soapClient.send(sslTrustAllCerts:true) {
            envelopeAttributes "xmlns:tns": "http://service.ws.hw.com/"
            body {
                'tns:getReader' {
                    'arg0'(un)
                    'arg1'(d)
                    'arg2'(v)
                    'arg3'(type)
                    'arg4'(readerId)
                }
            }
        }

        convertToMap(response.getReaderResponse.return)
    }
    
    def getBook(int type, String bookId) {

	    String d = sdate()
        String v = verify(d)
        
        def response = soapClient.send(sslTrustAllCerts:true) {
            envelopeAttributes "xmlns:tns": "http://service.ws.hw.com/"
            body {
                'tns:getBook' {
                    'arg0'(un)
                    'arg1'(d)
                    'arg2'(v)
                    'arg3'(type)
                    'arg4'(bookId)
                }
            }
        }

        convertToMap(response.getBookResponse.return)
    }

    def getCircs(String certId) {
        String d = sdate()
        String v = verify(d)
        
        def response = soapClient.send(sslTrustAllCerts:true) {
            envelopeAttributes "xmlns:tns": "http://service.ws.hw.com/"
            body {
                'tns:getCircs' {
                    'arg0'(un)
                    'arg1'(d)
                    'arg2'(v)
                    'arg3'(certId)
                }
            }
        }

        convertToMap(response.getCircsResponse.return)
    }

    def getViolations(String certId) {
        String d = sdate()
        String v = verify(d)
        
        def response = soapClient.send(sslTrustAllCerts:true) {
            envelopeAttributes "xmlns:tns": "http://service.ws.hw.com/"
            body {
                'tns:getViolations' {
                    'arg0'(un)
                    'arg1'(d)
                    'arg2'(v)
                    'arg3'(certId)
                }
            }
        }

        convertToMap(response.getViolationsResponse.return)
    }

	def getDebts(String certId) {
        String d = sdate()
        String v = verify(d)
        
        def response = soapClient.send(sslTrustAllCerts:true) {
            envelopeAttributes "xmlns:tns": "http://service.ws.hw.com/"
            body {
                'tns:getDebts' {
                    'arg0'(un)
                    'arg1'(d)
                    'arg2'(v)
                    'arg3'(certId)
                }
            }
        }

        convertToMap(response.getDebtsResponse.return)
    }
	
    def getPregArrivals(String certId) {
        String d = sdate()
        String v = verify(d)
        
        def response = soapClient.send(sslTrustAllCerts:true) {
            envelopeAttributes "xmlns:tns": "http://service.ws.hw.com/"
            body {
                'tns:getPregArrivals' {
                    'arg0'(un)
                    'arg1'(d)
                    'arg2'(v)
                    'arg3'(certId)
                }
            }
        }

        convertToMap(response.getPregArrivalsResponse.return)
    }

    def getRelegateArrivals(String certId) {
        String d = sdate()
        String v = verify(d)
        
        def response = soapClient.send(sslTrustAllCerts:true) {
            envelopeAttributes "xmlns:tns": "http://service.ws.hw.com/"
            body {
                'tns:getRelegateArrivals' {
                    'arg0'(un)
                    'arg1'(d)
                    'arg2'(v)
                    'arg3'(certId)
                }
            }
        }

        convertToMap(response.getRelegateArrivalsResponse.return)
    }

    def getViolations1(String respWkr,String certId) {
        String d = sdate()
        String v = verify(d)
        
        def response = soapClient.send(sslTrustAllCerts:true) {
            envelopeAttributes "xmlns:tns": "http://service.ws.hw.com/"
            body {
                'tns:getViolations1' {
                    'arg0'(un)
                    'arg1'(d)
                    'arg2'(v)
                    'arg3'(respWkr)
                    'arg4'(certId)
                }
            }
        }

        convertToMap(response.getViolations1Response.return)
    }
}
