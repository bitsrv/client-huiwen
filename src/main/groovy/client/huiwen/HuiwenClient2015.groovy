package client.huiwen

import java.security.MessageDigest
import wslite.rest.RESTClient

class HuiwenClient2015 {

    String url
    String token
    
    def sdf
    def restClient

    HuiwenClient2015(String url, String token) {
        this.url = url
        this.token = token
        restClient = new RESTClient(url)
        restClient.httpClient.connectTimeout = 5000
        restClient.httpClient.readTimeout = 10000
        restClient.httpClient.useCaches = false
        restClient.httpClient.followRedirects = false
        restClient.httpClient.sslTrustAllCerts = true

    }

    private def convertToMap(nodes) {
        nodes.children().collectEntries { 
            [ it.name(), it.childNodes() ? convertToMap(it) : it.text() ] 
        }
    }

    def getReader(String readerId) {
    
		def resp = restClient.get(path:"m/api/rinfo.php",
                       query:["certId":readerId,"token":token])

        return resp?.json
    }
    
	def getMyLibrary(String readerId) {
    
		def resp = restClient.get(path: "m/api/rlend.php",
                                query:["certId":readerId,"token":token])

        return resp?.json
    }
   
	
	def queryBook(String grp, String q, int offset) {
    
		def resp = restClient.get(path:"m/api/search.php",
                        query:["grp":grp,"q":q,"start":offset,"token":token])

        return resp?.json
    }
	
	def getBookInfo(String callNo) {
    
		def resp = restClient.get(path:"m/api/detail.php",
                        query:["id":callNo,"token":token])

        return resp?.json
    }
	
	def rLoss(String badgenumber,String userpwd) {
    
		def resp = restClient.get(path:"m/api/rloss.php",
                            query:["certId":badgenumber,"pwd":userpwd,"token":token])

        return resp?.json
    }
	
	def rLogin(String badgenumber,String userpwd) {
    
		def resp = restClient.get(path:"m/api/rlogin.php",
                                                query:["name":badgenumber,"passwd":userpwd,"token":token])

        return resp?.json
    }
	
	def rRenew(String badgenumber,String barcode) {
    
		def resp = restClient.get(path:"m/api/rrenew.php",
                        query:["barcode":barcode,"certId":badgenumber,"token":token])

        return resp?.json
    }
}
